# PACT Showcase #

## Preconditions ##

Following Tools must be available on your client:

- Docker + Docker Compose
- JDK >= 1.8
- Maven

## Build ##

Start pact broker:

```docker-compose up -d```

Run Maven Package:

```mvn clean package```

## What do you see ##
The Maven Project includes 3 modules

### provider Module ###
This module contains a service that exposes a REST resource

### consumer Module ###
This module containse a service for consuming the customer REST resource of the provider

### pact-verifier Module ###
This module verifies the interface contract between consumer and provider against a running provider instance

To run this module you need a running instance of the provider:

```java -jar ./provider/target/provider-1.0.0.RELEASE.jar```

and then run the verify goal on this module:

```mvn pact:verify -pl pact-verifier```