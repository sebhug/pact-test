package de.sebhug.test.pact;

import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.PactVerification;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.RequestResponsePact;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerClientGetTest extends ProviderRule {

    @Autowired
    CustomerClient serviceUnderTest;

    private CustomerResource body = CustomerResource.builder()
            .oid(UUID.fromString("542c000c-e7b3-489a-ae25-fc2d24f4d76f"))
            .name("Franz Kafka")
            .version(1L)
            .build();

    @Pact(consumer = "pact-consumer", provider = "pact-provider", state = "Existing Franz Kafka user with OID: 542c000c-e7b3-489a-ae25-fc2d24f4d76f")
    public RequestResponsePact getCustomer(PactDslWithProvider builder) {
        return builder
                .given("An existing Customer: Franz Kafka")
                .uponReceiving("GET Customer by OID")
                .method(HttpMethod.GET.toString())
                .path(String.format("/customers/%s", body.getOid()))
                .willRespondWith()
                .status(HttpStatus.OK.value())
                .body(getJsonFromCustomer(body), MediaType.APPLICATION_JSON_VALUE)
                .toPact();
    }

    @Test
    @PactVerification(fragment = "getCustomer")
    @DirtiesContext
    public void getTest() throws Exception {
        CustomerResource response = serviceUnderTest.getCustomer(body.getOid());
        Assertions.assertThat(response).isEqualTo(body);
    }


}
