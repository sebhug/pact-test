package de.sebhug.test.pact;

import au.com.dius.pact.consumer.PactProviderRuleMk2;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.Rule;

public class ProviderRule {

    @Rule
    public PactProviderRuleMk2 mockProvider =
            new PactProviderRuleMk2("pact-provider", "localhost", 8081, this);

    @SneakyThrows
    public String getJsonFromCustomer(CustomerResource resource) {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(resource);
    }
}
