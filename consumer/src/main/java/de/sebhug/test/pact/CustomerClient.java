package de.sebhug.test.pact;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

@Service
public class CustomerClient {

    private final RestTemplate providerRestTemplate;

    @Autowired
    public CustomerClient(RestTemplate providerRestTemplate) {
        this.providerRestTemplate = providerRestTemplate;
    }

    public CustomerResource getCustomer(UUID oid) {
        return providerRestTemplate.getForObject(String.format("/customers/%s", oid), CustomerResource.class);
    }

    public CustomerResource putCustomer(CustomerResource body) {
        HttpEntity<CustomerResource> entity = new HttpEntity<>(body);
        return providerRestTemplate.exchange(String.format("/customers/%s", body.getOid()), HttpMethod.PUT, entity, CustomerResource.class).getBody();
    }
}
