package de.sebhug.test.pact;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class ConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class, args);
    }

    @Value("${provider.url}")
    private String PROVIDER_URI;

    @Bean
    public RestTemplate providerRestTemplate() {
        return new RestTemplateBuilder().rootUri(PROVIDER_URI).build();
    }
}
