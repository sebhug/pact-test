package de.sebhug.test.pact;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping(value = "/customers", produces = MediaType.APPLICATION_JSON_VALUE)
public class CustomerRestController {

    @GetMapping(path = "/{oid}")
    public CustomerResource getCustomer(@PathVariable("oid") UUID oid) {
        return CustomerResource.builder()
                .oid(oid)
                .name("Franz Kafka")
                .version(1L)
                .build();
    }

    @PutMapping(path = "/{oid}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public CustomerResource updateCustomer(@PathVariable("oid") UUID oid, @RequestBody CustomerResource body) {
        body.setOid(oid);
        body.setVersion(body.getVersion() + 1L);
        return body;
    }

}
